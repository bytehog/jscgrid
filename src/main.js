'use strict';

var idCount = 0;

function JSCGrid(config) {
    config = config || {};

    if (!config.container || !config.container.parentNode) {
        throw "JSCGrid must have a valid container!";
    }

    var id = this._id = 'jscgrid' + (++idCount);

    var container = this.container = config.container;
    this.configuration = config.configuration;

    var canvas = this._canvas = document.createElement('canvas');
    var containerRect = container.getBoundingClientRect();

    var width = this._width = this._containerWidth = containerRect.width;
    var height = this._height = this._containerHeight = containerRect.height;

    canvas.setAttribute('width', width);
    canvas.setAttribute('height', height);
    canvas.setAttribute('style', 'display:block;position:fixed;left:0;top:0;')

    var wrapper = this._wrapper = document.createElement('div');
    var wrapperStyle = this._wrapperStyle = wrapper.style;

    wrapper.setAttribute('id', id);
    wrapperStyle.position = 'relative';
    wrapperStyle.width = width + 'px';
    wrapperStyle.height = height + 'px';
    wrapperStyle.overflow = 'visible';

    container.style.overflow = 'scroll';

    wrapper.appendChild(canvas);
    container.appendChild(wrapper);

    this._ctx = canvas.getContext('2d');

    this._drawCallback = this._draw.bind(this);
}

JSCGrid.prototype = {
    link: function (data) {
        if (Array.isArray(data)) {
            data.forEach(function(item) {
                Object.defineProperty(item, '_id', {writable: false, configurable: false, enumerable: false, value: 'row' + (++idCount)});
            });

            if (this.configuration.style.width !== 'fill') {
                this._width = this.configuration.style.width;
                this._wrapperStyle.width = this._width + 'px';
            }

            if (this.configuration.style.height !== 'fill') {
                this._height = this.configuration.style.height * data.length;
                this._wrapperStyle.height = this._height + 'px';
            }

            this._data = data;
        }
    },

    show: function () {
        requestAnimationFrame(this._drawCallback);
    },

    _draw: function () {
        var rowConf = this.configuration;
        var fieldsConf = rowConf.row.fields;
        var start = Math.floor(this.container.scrollTop / this._height * this._data.length);
        var requestedEls = Math.floor(this._containerHeight / rowConf.style.height);
        var end = start + requestedEls >= this._data.length ? this._data.length : start + requestedEls;
        var ctx = this._ctx;
        var fieldConf;
        var row;
        var x = 0;
        var y = 0;

        ctx.save();
        for (var i = start; i < end; i++) {
            row = this._data[i];

            ctx.save();
            for (var c = 0; c < fieldsConf.length; ++c) {
                fieldConf = fieldsConf[c];

                ctx.fillStyle = 'rgba(' + rowConf.style.background.join(',') + ')';
                ctx.fillRect(0, 0, fieldConf.style.width, rowConf.style.height);

                ctx.font = '10px sans-serif';
                ctx.fillStyle = 'rgba(' + rowConf.style.foreground.join(',') + ')';
                ctx.fillText(row[fieldConf.name].toString(), 0, rowConf.style.height / 2, fieldConf.style.width);
                
                ctx.translate(fieldConf.style.width, fieldConf.style.y);
            }
            ctx.restore();

            ctx.translate(0, rowConf.style.height);
        }
        ctx.restore();

        requestAnimationFrame(this._drawCallback);
    }
};

window.JSCGrid = JSCGrid;
