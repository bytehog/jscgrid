window.addEventListener('load', function () {
    var container = document.getElementById('gridContainer');

    // generate some random data
    function rand(min, max) {
        return Math.floor(((Math.random() * max) + min));
    }
    var NAMES = ['mbank', 'idea', 'orange', 'era', 'p4', 'gg', 'pko', 'lot'];
    var data = [];

    for (var i = 0; i < 1000; ++i) {
        data.push({
            'name': NAMES[rand(0, NAMES.length)],
            'price1': parseFloat(rand(0, 1000).toFixed(2)),
            'price2': parseFloat(rand(0, 500).toFixed(2)),
            'currency': rand(1, 2) === 1 ? 'usd' : 'euro',
            'operationDate': new Date(rand(0, Date.now()))
        });
    }

    var grid = new window.JSCGrid({
        container: container,
        configuration: {
            style: {
                width: 'fill',
                height: 40,
                x: 0,
                y: 'add',
                background: [0, 0, 0, 1],
                foreground: [255, 255, 255, 1]
            },
            row: {
                fields: [
                    {
                        name: 'name',
                        display: 'nazwa',
                        type: 'string',
                        style: {
                            x: 'add',
                            y: 0,
                            width: 100,
                            height: 'fill'
                        }
                    },
                    {
                        name: 'price1',
                        display: 'cena zakupu',
                        type: 'number',
                        style: {
                            x: 'add',
                            y: 0,
                            width: 20,
                            height: 'fill'
                        }
                    },
                    {
                        name: 'price2',
                        display: 'cena sprzedaży',
                        type: 'number',
                        style: {
                            x: 'add',
                            y: 0,
                            width: 50,
                            height: 'fill'
                        }
                    },
                    {
                        name: 'currency',
                        display: 'waluta',
                        type: 'string',
                        style: {
                            x: 'add',
                            y: 0,
                            width: 50,
                            height: 'fill'
                        }
                    },
                    {
                        name: 'operationDate',
                        display: 'data',
                        type: 'date',
                        style: {
                            x: 'add',
                            y: 0,
                            width: 250,
                            height: 'fill'
                        }
                    }
                ]
            }
        }
    });

    grid.link(data);
    grid.show();
});
