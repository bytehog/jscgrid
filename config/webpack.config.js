const path = require('path');
const webpack = require('webpack');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const CopyWebpackPlugin = require('copy-webpack-plugin');

module.exports = (env, opts) => {
    const config = {
        entry: {
            jscgrid: path.resolve(__dirname, '../src/main.js'),
        },
        mode: opts.mode || 'development',
        resolve: {
            extensions: ['.js']
        },
        output: {
            filename: '[name].js',
            path: path.resolve(__dirname, '../build')
        },
        optimization: {},
        plugins: []
    };

    if (opts.mode && opts.mode === 'development') {
        config.devtool = 'inline-source-map';

        if (opts.serve) {
            config.devServer = {
                contentBase: path.resolve(__dirname, '../build'),
                compress: true,
                port: 9000
            }
        }
    } else {
        config.plugins.push(new webpack.optimize.UglifyJsPlugin())
    }

    config.plugins.push(new CopyWebpackPlugin([
        {from: './src/example/**.j*', to: '../build/', flatten: true},
    ]));

    config.plugins.push(new HtmlWebpackPlugin({
        template: '!!html-loader!./src/example/example.html',
        filename: 'example.html'
    }));

    return config;
};